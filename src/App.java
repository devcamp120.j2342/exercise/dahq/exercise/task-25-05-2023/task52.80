import java.text.DecimalFormat;
import java.util.Random;

public class App {
    public static void main(String[] args) throws Exception {
        // task1
        System.out.println("task1: " + task1(1234, 4));
        // task2
        System.out.println("Task2: " + task2(1, 10));
        // task3
        System.out.println("Task3: " + task3(3, 4));
        // task4
        System.out.println(("Task4: " + task4(94)));
        // task5
        System.out.println("Task5: " + task5(32));
        System.out.println("Task5: " + task5(137));
        // task6
        System.out.println("Task6: " + task6(123));
        System.out.println("Task6: " + task6("abc "));
        // task7
        System.out.println("Task7 :" + task7(19));
        System.out.println("Task7 :" + task7(8));
        // task8
        System.out.println("Task8: " + task8(-19));
        System.out.println("Task8: " + task8(19));
        System.out.println("Task8: " + task8(1.9));
        // task10
        task10(8);
        task10(10);
    }

    public static String task1(double x, int n) {
        double roundedX = Math.round(x * (Math.pow(10, n))) / Math.pow(10.0, n);
        DecimalFormat df = new DecimalFormat("#.000");
        String formattedX = df.format(roundedX);

        return formattedX;
    }

    public static int task2(int x, int y) {
        Random rand = new Random();
        int randomNumber = rand.nextInt((y - x) + 1) + x;
        return randomNumber;
    }

    public static float task3(float x, float y) {
        return (float) Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public static boolean task4(int x) {
        if (Math.pow(Math.sqrt(x), 2) == x) {
            return true;
        }
        return false;
    }

    public static int task5(int x) {

        while (x % 5 != 0) {
            x++;
        }

        return x;
    }

    public static boolean task6(Object obj) {
        return obj instanceof Number;
    }

    public static boolean task7(int n) {
        boolean flag = false;
        for (int i = 0; i < Math.sqrt(n) + 1; i++) {
            if (Math.pow(2, i) == n) {
                flag = true;
            }
        }
        return flag;

    }

    public static boolean task8(Number n) {

        if (n instanceof Integer && n.intValue() > 0) {
            return true;

        }

        return false;
    }

    public static void task10(int n) {

        String binaryNumber = Integer.toBinaryString(n);
        System.out.println("The binary representation of " + n + " is " + binaryNumber);
    }
}
